#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

/*
    schelet pentru exercitiul 5
*/

int* arr;
int array_size;
int r;


typedef struct{
    int start;
    int end;
    int id;
}Thread_arguments;

void *f(void *arg) {
    Thread_arguments *thread_arg = (Thread_arguments *) arg;
    for (int i = thread_arg->start;i< thread_arg->end;i++)
            arr[i] += 100;
    pthread_exit(NULL);
}

int main(int argc, char *argv[]) {
    int numar;
    if (argc < 2) {
        perror("Specificati dimensiunea array-ului\n");
        exit(-1);
    }

    array_size = atoi(argv[1]);
    numar = atoi(argv[2]);

    arr = malloc(array_size * sizeof(int));
    for (int i = 0; i < array_size; i++) {
        arr[i] = i;
    }

    for (int i = 0; i < array_size; i++) {
        printf("%d", arr[i]);
        if (i != array_size - 1) {
            printf(" ");
        } else {
            printf("\n");
        }
    }

    // TODO: aceasta operatie va fi paralelizata
    pthread_t threads[numar];
    Thread_arguments thread_arg[numar];
  	for (int id = 0; id < numar; id++) {
        int start = id * (double)array_size / numar;
        int end;
        if ((id+1) * (double)array_size / numar > array_size)
        {
            end = array_size;
        }
        else
        {
            end = (id + 1) * (double)array_size / numar;
        }
        thread_arg[id].id = id;
        thread_arg[id].start = start;
        thread_arg[id].end = end;

        r = pthread_create(&threads[id], NULL, f, &thread_arg[id]);

        if (r) {
            printf("Eroare la asteptarea thread-ului %d\n", id);
            exit(-1);
        }
    }

    void *status;
    for (int id = 0; id < numar; id++) {
        r = pthread_join(threads[id], &status);

        if (r) {
            printf("Eroare la asteptarea thread-ului %d\n", id);
            exit(-1);
        }
    }

    for (int i = 0; i < array_size; i++) {
        printf("%d", arr[i]);
        if (i != array_size - 1) {
            printf(" ");
        } else {
            printf("\n");
        }
    }

  	pthread_exit(NULL);
}
