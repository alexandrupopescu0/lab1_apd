#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define NUM_THREADS 2

void *f(void *arg) {
  	long id = (long)arg;
  	for (int i = 0; i < 99;i++){
  		printf("Indice iteratie: %d, Hello World din thread-ul %ld!\n", i, id);
  	}
  	pthread_exit(NULL);
}

void *g(void *arg) {
  	long id = (long)arg;
  	printf("Hello World din thread-ul %ld!\n", id);
  	pthread_exit(NULL);
}

int main(int argc, char *argv[]) {
	///pt ex 2 am modificat for urile sa mearga pana la cores si
	///instanta de threads sa fie de forma threads[cores] la ex 4 
	///spune sa am 2 threaduri de aceea am modificat/comentat aceste instructiuni
	long cores = sysconf(_SC_NPROCESSORS_CONF);
	pthread_t threads[NUM_THREADS];
  	int r;
  	long id;
  	void *status;

  	for (id = 0; id < NUM_THREADS; id++) {
  		if (id  % 2 == 1)
			r = pthread_create(&threads[id], NULL, f, (void *)id);
		else
			r = pthread_create(&threads[id], NULL, g, (void *)id);


		if (r) {
	  		printf("Eroare la crearea thread-ului %ld\n", id);
	  		exit(-1);
		}
  	}

  	for (id = 0; id < NUM_THREADS; id++) {
		r = pthread_join(threads[id], &status);

		if (r) {
	  		printf("Eroare la asteptarea thread-ului %ld\n", id);
	  		exit(-1);
		}
  	}

  	pthread_exit(NULL);
}
